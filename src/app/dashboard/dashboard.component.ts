import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
valnote: string;
  constructor(private serv: AuthService) { }

  ngOnInit() {
  }

  addNote(){
    const user = {
      note: this.valnote
    };
    this.serv.postNote(user).subscribe(res =>{
      console.log("ok");
    });
  }

}
