import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthGuard } from './auth.guard';
import { LogOutComponent } from './log-out/log-out.component';



const routes: Routes = [{path : 'register', component : RegisterComponent},
{path : 'login', component : LoginComponent},
{path : 'dashboard', component : DashboardComponent,  canActivate: [AuthGuard]},
{path : 'log-out', component : LogOutComponent}
];



@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
