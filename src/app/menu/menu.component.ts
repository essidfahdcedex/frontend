import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router, CanActivate} from '@angular/router';


@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  constructor(private serv: AuthService , private router : Router) { }

  ngOnInit() {
  }
  logOut() {
    localStorage.clear();
    this.router.navigate(['login']);
  }
}
