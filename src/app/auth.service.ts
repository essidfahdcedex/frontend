import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as File from './common/file';
import { Router } from '@angular/router';



@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient, private router : Router) {
  }


  getRegister(user) {
    return this.http.post('http://localhost:3000/register', user);
  }
  getLogin(user) {
    return this.http.post<any>('http://localhost:3000/login', user);
  }
  saveUser(token) {
    localStorage.setItem(File.AUTH_TOKEN, token);
  }
  postNote(user) {
    return this.http.post('http://localhost:3000/todo/:id/:i', user);
  }


}
