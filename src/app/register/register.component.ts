import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  ok: string;
  ko: string;
  nameErr: boolean;
  lastnameErr: boolean;
  emailErr: boolean;
  pass1Err: boolean;
  pass2Err: boolean;

  valname: string;
  vallastname: string;
  valemail: string;
  valpass1: string;
  valpass2: string;

  personne = {
    firstname: '',
    lastname: '',
    emailaddress: '',
    pwd1: '',
    pwd2: ''
  };

  //tab = [{}];

  constructor(private serv: AuthService) { }
  ngOnInit() {
  }
  verifcolor(n, l, e, p1, p2) {

    if (n === '') {
      this.nameErr = true;
    } else { this.nameErr = false; }

    if (l === '') {
      this.lastnameErr = true;
    } else { this.lastnameErr = false; }

    if (e === '') {
      this.emailErr = true;
    } else { this.emailErr = false; }

    if (p1 === '') {
      this.pass1Err = true;
    } else { this.pass1Err = false; }

    if (p2 === '') {
      this.pass2Err = true;
    } else { this.pass2Err = false; }

  }
  sendDB() {
    if (!this.nameErr && !this.lastnameErr && !this.emailErr && !this.pass1Err && !this.pass2Err) {
      this.ok = this.valname + this.vallastname + this.valemail + this.valpass1 + this.valpass2;
      this.personne.firstname = this.valname;
      this.personne.lastname = this.vallastname;
      this.personne.emailaddress = this.valemail;
      this.personne.pwd1 = this.valpass1;
      this.personne.pwd2 = this.valpass2;
      // this.tab[0] = this.personne;
      // this.ko = this.tab[0].personne.firstname;
    }
  }


  private newMethod(): any {
    return 1;
  }
  regUser() {
    const user = {
      login: this.vallastname,
      password: this.valpass1,
      email: this.valemail,
    };

    this.serv.getRegister(user).subscribe(res =>{
      console.log("ok");
    });

  }
}


