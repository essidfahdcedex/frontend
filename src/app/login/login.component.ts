import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router, CanActivate } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  valemaillog: string;
  valpwdlog: string;
  auth: any;
  constructor(private serv: AuthService, private router: Router) { }

  ngOnInit() {
  }
  logUser() {
    const user = {
      email: this.valemaillog,
      password: this.valpwdlog
    };
    console.log(user);
    this.serv.getLogin(user).subscribe((res: any) => {
      console.log(res);

      this.serv.saveUser(res.token);

      this.router.navigate(['/dashboard']);
      localStorage.setItem('token', res.token)
    });

  }

}
